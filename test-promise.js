//A Promise is an object that represents the result of an asynchronous function call
//If a function cannot return a value or throw an exception without blocking, it can return 
//a promise instead. A promise is an object that represents the return value or the thrown exception that the function may eventually provide

var Promise = require('promise');
var Q = require('q');


var promise = new Promise(function (resolve, reject) {
	// resolve('bla')
	throw 'ERROR'
});


var greetingPromise = promise;
greetingPromise.then(function (greeting) {
    return greeting + '!!!!';
}).then(function (greeting) {
    console.log(greeting);    // 'hello world!!!!’
}).catch(function (err) {
	console.log(err)
});

function sayHello() {
	return 'HELLO'
}

Q.fcall(sayHello).then(function (value) {
	console.log('VALUE:', value)
})