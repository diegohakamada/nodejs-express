var amqp = require('amqplib');
var url = 'amqp://localhost';

amqp.connect(url).then(function(conn) {

    return conn.createChannel().then(function(ch) {
        return ch.assertExchange('DeadLetterExchange', 'direct').then(function() {
            return ch.assertQueue('DeadLetterQueue', {
                autoDelete: false,
                durable: true,
                arguments: {
                    'x-dead-letter-exchange': 'WorkExchange',
                    'x-dead-letter-routing-key' : '#1',
                    'x-message-ttl' : 60000
                }
            }).then(function() {
                return ch.bindQueue('DeadLetterQueue', 'DeadLetterExchange', '#2');
            })
        })
    }).then(function () {
        //Subscribe to the WorkQueue in WorkExchange to which the "delayed" messages get dead-letter'ed (is that a verb?) to.
        return conn.createChannel().then(function(ch) {
            return ch.assertExchange('WorkExchange', 'direct')
            .then(function() {
                return ch.assertQueue('WorkQueue', {
                    autoDelete: false,
                    durable: true,
                    arguments: {
                        'x-dead-letter-exchange': 'DeadLetterExchange',
                        'x-dead-letter-routing-key' : '#2'
                    }
                })
            }).then(function() {
                return ch.bindQueue('WorkQueue', 'WorkExchange', '#1');
            }).then(function() {
                console.log('Waiting for consume.');

                return ch.consume('WorkQueue', function(msg) {
                    console.log('Received and nack message: ', msg.content.toString());
                    ch.reject(msg, false);
                });
            });
        })
    }).then(function () {
        //Now send a test message to DeadExchange to a random (unique) queue.
        return amqp.connect(url).then(function(conn) {
            return conn.createChannel().then(function(ch) {
                return ch.checkExchange('WorkExchange').then(function() {
                     return ch.checkQueue('WorkQueue');


                    // return ch.assertQueue('WorkQueue', {
                    //     autoDelete: false,
                    //     durable: true,
                    //     arguments: {
                    //         'x-dead-letter-exchange': 'DeadLetterExchange',
                    //         'x-dead-letter-routing-key' : '#',
                    //         'x-message-ttl' : 1000
                    //     }
                    })

                    .then(function(ok) {
                        console.log('Sending message');

                        return ch.sendToQueue(ok.queue, new Buffer(':)'));
                    });
                })
            })
        })
    // })
}).then(null, function(error) {
    console.log('error\'ed')
    console.log(error);
    console.log(error.stack);
});