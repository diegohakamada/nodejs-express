var amqp = require('amqplib');
var AMQPUtil = require('./lib/amqp-util')
var path = require('path');
var options = require('konfig')({ path: path.resolve('./test/config')}).rabbitmq;

var amqpUtil = new AMQPUtil(amqp, options);
amqpUtil.configureExchange(function (err) {
	console.log('Configurado - err:', err);
	return;
});
