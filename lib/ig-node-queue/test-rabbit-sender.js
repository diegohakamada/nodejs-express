var amqp  = require('amqplib');
var RabbitmqSender = require('./lib/rabbitmq-sender');
var path = require('path');
var options = require('konfig')({ path: path.resolve('./test/config')}).rabbitmq;
var teste = new RabbitmqSender(amqp, options);

teste.send('{ "id": "1", "timeout" : "15000", "action" : "resume" }', function (err) { console.log('ERROR:', err)});
teste.send('{ "id": "2", "timeout" : "15000", "action" : "resume" }', function (err) { console.log('ERROR:', err)});

module.exports = RabbitmqSender;