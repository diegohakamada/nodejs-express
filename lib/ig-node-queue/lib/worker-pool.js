var cluster 		= require('cluster');

/**
 * @class WorkerPool
 * 
 * Manages a set of worker process for client job processing
 *
 * @param {String} workerPath Path to worker js file
 * @param {Number} maxWorkers Maximum number of worker processes to keep active
 * @param {object=} options	  Options
 * 	   @param {object=} env 			ENV variables to set on each worker at launch
 * 	   @param {Number} workerTimeout 	Timeout (ms) to disconnect and replace a worker
 *         after sending it a job 	                                
 *
 * @todo refactor to reduce size of methods
 * 
 * @author Ricardo Massa 
 */
function WorkerPool (workerPath, maxWorkers, options) {
	this._maxWorkers = maxWorkers;
	this._workerPath = workerPath;
	this._options = WorkerPool._normalizeOptions(options);

	this._workerPath = workerPath;
	this._workerMap  = {};
	this._workerPool = [];
	this._workerRequests = {};
	this._workerCount = 0;
	this._listeners = {};
	this._closed = false;

	this._setupWorkers();
}

//Inherit the prototype methods from one constructor to WorkerPool
// util.inherits(WorkerPool, events.EventEmitter);

/**
 * Fills the optional pool parameters with default values
 * 		
 * @param  {object} userOptions (Probably incomplete) user supplied options
 * @return {object}             A complete options object
 */
WorkerPool._normalizeOptions = function (userOptions) {
	var opts = userOptions || {};
	opts.env = opts.env || {};
	opts.workerTimeout = opts.workerTimeout || 60000; //TODO remover
	return opts;
}

/**
 * Initialize the worker pool (init processes)
 * 
 * @return {void} 
 */
WorkerPool.prototype._setupWorkers = function () {
	this._setupCluster();
}

/**
 * Configure cluster constants and worker behavior handling
 * 
 * @return {void} 
 */
WorkerPool.prototype._setupCluster = function () {
	var self = this;

	cluster.setupMaster({exec: this._workerPath});

	this._listeners.online = function (worker) {
		console.info('Worker ' + worker.id + ' online');
	}

	cluster.on('online', this._listeners.online);

	this._listeners.disconnect = function (worker) {
		console.log('############### worker disconnect')
		if (typeof self._workerMap[worker.id] == 'undefined') {
			console.warn('Worker', worker.id, 'disconnected but it was not spawned by this pool');
			return;
		}

		self._workerCount--;

		console.info('Worker ' + worker.id + ' disconnected');
		var id = worker.id;

		self._unregisterWorker(worker);

		var request = self._workerRequests[id];
		if (request) {
			clearTimeout(request.timer);
			worker.removeListener('message', request.messageListener);
			var cb = request.callback;
			delete(request[id]);
			cb(new Error('Worker disconnected'));
		}		
	}

	cluster.on('disconnect', this._listeners.disconnect);
}

/**
 * Spawns a new worker process
 * 
 * @return {void}
 */
WorkerPool.prototype._spawnWorker = function (cb) {
	var self = this;

	this._workerCount++;

	console.info('Spawning a worker');

	var worker = cluster.fork(this._options.env);

	/*
	 * While this worker doesn't get ready, it is not pushed
	 * into _workerPool (thus its index is -1)
	 */
	this._workerMap[worker.id] = -1;

	worker.once('message', function (msg) {
		if (msg.event != 'ready') {
			worker.disconnect();
			return;
		}
		console.info('Worker', worker.id, 'is ready to receive jobs');
		cb(worker);
	})
}

/**
 * Drops a worker (likely to be tainted by an exception or timeout)
 * from the pool
 * 
 * @param  {Worker} worker Worker to drop
 * @return {void}
 */
WorkerPool.prototype._unregisterWorker = function (worker) {
	console.info('Unregistering worker ' + worker.id);
	var oldId = worker.id;
	var index = this._workerMap[oldId];
	delete this._workerMap[oldId]
	if (index > -1) {
		this._workerPool.splice(index, 1);
	}
}

/**
 * Shutdown idle workers and stops accepting process requests.
 * Currently busy workers are also shutdown after returning.
 * 
 * @return {void}
 */
WorkerPool.prototype.shutdown = function () {
	if (this._closed) return;
	this._closed = true;
	for (var i = 0; i < this._workerPool.length; i++)
		this._workerPool[i].disconnect();
	cluster.removeListener('online', this._listeners.online);
	cluster.removeListener('disconnect', this._listeners.disconnect);
}

/**
 * Attempts to process a client message using an available worker
 * 
 * @param  {mixed}   msg  Message to process
 * @param  {Function} cb  Callback
 * @return {void}
 */
WorkerPool.prototype.process = function (msg, cb) {
	var self = this;

	if (this._closed) {
		cb(new Error('The pool cannot process requests after shutdown'));
		return;
	}

	var worker = this._workerPool.pop();
	if (!worker) {
		if (this._workerCount == this._maxWorkers) {
			var msg = 'No worker available to process message ' + JSON.stringify(msg); 
			console.error(msg);
			cb(new Error(msg));
			return;	
		}

		this._spawnWorker(function (worker) {
			self._sendJob(worker, msg, cb);
		})
		return;
	}

	this._sendJob(worker, msg, cb)
}

WorkerPool.prototype._sendJob = function (worker, msg, cb) {
	var self = this;

	console.info('Dispatching message ', (msg && msg.content) ? msg.content.toString() : msg, ' to worker ' + worker.id);

	this._workerMap[worker.id] = -1;

	var messageListener = function (message) {
		if (message.event != 'done')
			return;

		worker.removeListener('message', messageListener);

		clearTimeout(timer);
		delete self._workerRequests[worker.id];

		if (self._closed) {
			self._unregisterWorker(worker);

		} else if (self._workerMap[worker.id] === -1) {
			self._workerPool.push(worker);
			self._workerMap[worker.id] = self._workerPool.length - 1;
		}

		cb(message.error, message.result);
	}

	worker.on('message', messageListener)

	var timer = setTimeout(function () {
		if (self._workerMap[worker.id] === -1) {
			console.warn('Worker ' + worker.id + ' is taking too long to respond - killing it');
			worker.kill();	
		}
	}, this._options.workerTimeout);

	this._workerRequests[worker.id] = {
		callback: cb,
		messageListener: messageListener,
		timer: timer
	}

	worker.send(msg);
}


WorkerPool.prototype.isFull = function () {
	return (this._workerPool.length == 0 && this._workerCount == this._maxWorkers);
}

WorkerPool.prototype.isEmpty = function () {
	return !Object.keys(this._workerRequests).length;
}

/* 
 * For compliance with wire module
 */
module.exports = function () {
	var pool = Object.create(WorkerPool.prototype);
	WorkerPool.apply(pool, arguments);
	return pool;
}; 

module.exports.WorkerPool = WorkerPool;