var logger 			= require('./log');
var util 			= require("util");
var Readable 		= require('stream').Readable;

//TODO remove
logger.level = 'debug';

//Inherit the prototype methods from Readable to RabbitStrategy
util.inherits(RabbitStrategy, Readable);

/**
 * @class RabbitStrategy
 *
 * Agent (RabbitMQ) which listens for new messages arriving at a queue, 
 * and push to a stream (Readable).
 *
 * @constructor
 * @param {options}  options - Supplied options to consumer.
 *
 * @author Diego Hakamada
 */

function RabbitStrategy (amqp, options) {
	if (!(this instanceof RabbitStrategy))
				return new RabbitStrategy(options);

	Readable.call(this, { objectMode : true });

	var self = this;
	self._amqp = amqp;
	self._conn = null;
	self._channel = null;
	self._timer = null;
	self._isReconnecting = false;
	self._state = null;
	self._asked	= false;
	self._currentChannelId = -1;
	self._options = RabbitStrategy.normalizeOptions(options);
}

/**
 * Fills the optional consumer parameters with default values.
 * 		
 * @param  {object}  options - Supplied options.
 * @return {object}            A complete options object
 */

RabbitStrategy.normalizeOptions = function (options) {
	var opts = options || {};
	opts.amqp.backoffTime = opts.amqp.backoffTime || 60000;
	opts.amqp.channelTimeout = opts.amqp.channelTimeout || 60000;
	return opts;
}

/**
 * Start connection with AMQP and channel, after consuming the queue.
 * 		
 * @param  {Function}  handleMessageResult - function responsible to handle message result.
 * @return {void}
 */

RabbitStrategy.prototype.startConsuming = function () {
	var self = this;
	self._state = 'connecting';
	self._changeState('connected');
}

/**
 * Change state.
 * 		
 * @param  {String}   newState
 * @return {void}
 */

RabbitStrategy.prototype._changeState = function(newState) {
	var self = this;

	switch (newState) {
		case 'connected':
			if(self._state != 'connecting') {
				logger.debug('Invalid state condition');
				return;
			}

			self._enterStateConnected();
			break;

		case 'ready':
			if (self._state != 'connected') {
				logger.debug('Invalid state condition');
				return;
			}
			self._enterStateReady()
			break;

		case 'receiving':
			if(self._state != 'ready') {
				logger.debug('Invalid state condition');
				return;
			}

			self._enterStateReceiving();
			break;
		 
		 default:
        	logger.debug('Invalid state condition');
	}
}

/**
 * Enter state to ready.
 * 		
 * @param  {void}
 * @return {void}
 */

RabbitStrategy.prototype._enterStateReady = function() {
	var self = this;
	self._state = 'ready';
	self._isReconnecting = false;

	if (self._asked) {
		logger.info('RabbitStrategy[enterStateReady] - Start receiving');
		return self._changeState('receiving');
	}
}

/**
 * Enter state to receiving.
 * 		
 * @param  {void}
 * @return {void}
 */

RabbitStrategy.prototype._enterStateReceiving = function () {
	var self = this;
	self._state = 'receiving';

	logger.info('RabbitStrategy[enterStateReceiving] - Resuming queue consuming...');

    return self._channel.consume(self._options.amqp.mainExchange.queue.name, handleMessage);

	function handleMessage (msg) {

		if(msg) {
			logger.info('RabbitStrategy[enterStateReceiving] - receive new message:', (msg && msg.content) ? msg.content.toString() : msg);
			self._asked = false;
			msg._channelId = self._currentChannelId;
			self.push(msg);
		}
	}
}

/**
 * Enter state to connected.
 * 		
 * @param  {void}
 * @return {void}
 */

RabbitStrategy.prototype._enterStateConnected = function() {
	var self = this;
	self._amqp.connect(self._options.amqp.host).then(function (conn) {

		logger.info('RabbitStrategy[enterStateConnected] - Connected in AMQP');

		self._state = 'connected';
		self._conn = conn;

		clearTimeout(self._timer);

	    conn.on('error', function (err) {
	    	  self._conn = null;
	          logger.error('RabbitStrategy[enterStateConnected] - Error in connection:', ( err && err.stack ) ? err.stack : err);
	          self._handleConnectionError();
	          return;
	    })

	    conn.on('close', function () {
	    	logger.info('RabbitStrategy[enterStateConnected] - Close connection');
	    })

	    self._createChannel();

	}).then(null, function (err) {
		logger.error('RabbitStrategy[enterStateConnected] - Connect failed:', ( err && err.stack ) ? err.stack : err);
	})
}

/**
 * Create channel with AMQP.
 * 		
 * @param  {object}	 conn - connection.
 * @return {object}	 ok   - promise object.
 */

RabbitStrategy.prototype._createChannel = function () {
	var self = this;

	try {
		self._conn.createChannel().then(function (ch) {

			self._currentChannelId++;
			self._channel = ch;

			ch.on('error', function (err) {
				logger.error('RabbitStrategy[createChannel] - Error in channel:', err);
			});

			ch.on('close', function () {
				logger.warn('RabbitStrategy[createChannel] - Close channel');
				self._channel = null;
				
				if(self._conn) {
					setTimeout(function () {
						self._createChannel();
					}, self._options.amqp.channelTimeout);
				}
			});

			ch.on('blocked', function (reason) {
				logger.warn('RabbitStrategy[createChannel] - Blocked channel:', reason);
			});

			ch.on('unblocked', function () {
				logger.warn('RabbitStrategy[createChannel] - Unblocked channel');
			});

			return ch.checkExchange(self._options.amqp.mainExchange.name)

			.then(function() {
	        	return ch.checkQueue(self._options.amqp.mainExchange.queue.name);
	        }).then(function () {
	        	return self._channel.bindQueue(self._options.amqp.mainExchange.queue.name, 
					                		   self._options.amqp.mainExchange.name, 
					                		   self._options.amqp.mainExchange.queue.pattern);
	        }).then(function() {
	        	self._changeState('ready');
	        }, function (rejected) {
	        	logger.debug('RabbitStrategy[createChannel] - rejected in channel:', rejected);
	        });
		})
	} catch (err) {
		logger.error('RabbitStrategy[createChannel] - Error to create channel:', ( err && err.stack ) ? err.stack : err);
		if(self._conn) {
			setTimeout(function () {
					self._createChannel();
			}, self._options.amqp.channelTimeout);			
		}
	}
}

/**
 * Method implemented of Stream.
 * 		
 * @param  {void}
 * @return {void}
 */

RabbitStrategy.prototype._read = function () {
	this._asked = true;
	this._changeState('receiving');
}

/**
 * Handle connection errors of connection and channel.
 * Whether autoReconnect is true, then attemps to reconnect.
 * 		
 * @param  {Error}  error - Error.
 * @return {void}
 */

RabbitStrategy.prototype._handleConnectionError = function () {
	var self = this;

	if (!self._isReconnecting && self._options.amqp.autoReconnect) {
		logger.debug('RabbitStrategy[handleConnectionError] - reconnect with amqp');

		self._isReconnecting = true;

		self._timer = setInterval( function () {
			self.startConsuming();
		}, self._options.amqp.backoffTime);

		self.startConsuming();
 	}
}

/**
 * Delete mensage from the queue.
 * 		
 * @param {object}      msg - Message to delete.
 * @param  {Function}   callback - Callback.
 * @return {void} 
 */

RabbitStrategy.prototype.ack = function (msg) {
	
	if (this._channel && msg._channelId == this._currentChannelId) {
		logger.info('RabbitStrategy[ack] - Sending the acknowledgement message:', this.convertMsgToString(msg));
		this._channel.ack(msg);
	} else {
		logger.warn('RabbitStrategy[ack] - Not possible to send the acknowledgement message:', this.convertMsgToString(msg));
	}
}

/**
 * Requeue mensage in the queue.
 * 		
 * @param  {object}  msg - Message to requeue in the queue.
 * @return {void}
 */

RabbitStrategy.prototype.nack = function (msg) {

	if (this._channel && msg._channelId == this._currentChannelId) {
		logger.info('RabbitStrategy[nack] - Sending the acknowledgement negative message:', this.convertMsgToString(msg));
		//@TODO unit tests for these arguments
		this._channel.nack(msg, false, false);
	} else {
		logger.warn('RabbitStrategy[nack] - Not possible to send the acknowledgement negative message:', this.convertMsgToString(msg));
	}
}

/**
 * Convert a mesage object to string.
 * 	
 * @param {Object} - msg - Object message.
 * return {String}         String message.
 */

RabbitStrategy.prototype.convertMsgToString = function (msg) {
	return (msg && msg.content) ? msg.content.toString() : '';
}

module.exports = RabbitStrategy;