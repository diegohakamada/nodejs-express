var logger 	        = require('./log');
var util            = require("util");
var events          = require("events");

/**
 * @class SQSStrategy
 * 
 * Agent (AWS SQS) which listens for new messages arriving at a queue, 
 * dispatching them to a message handler (QueueConsumer).
 * 
 * @param {QueueService}  service -	Queue service (eg. an instance of aws-sqs).
 * @param {object}        options - Options.
 *     @param {Number}  maxMessagesPerFetch - Max number of handlers running at same time.
 *     @param {Number}  queryInterval - Time (ms) interval between two queries to the queue.
 *
 * @author Ricardo Massa
 */	

function SQSStrategy (service, options) {
	if (!(this instanceof SQSStrategy))
			return new SQSStrategy(service, options);
	
	events.EventEmitter.call(this);

	this._service 			= service;
	this._options 			= SQSStrategy.normalizeOptions(options);
	this.stats 				= { currentCycle: -1 };
	this._idle 				= true;
	this._timer 			= null;
}

//Inherit the prototype methods from SQSStrategy to EventEmitter
util.inherits(SQSStrategy, events.EventEmitter);

/**
 * Fills the optional consumer parameters with default values.
 * 		
 * @param  {object}  options - (Probably incomplete) user supplied options.
 * @return {object}             A complete options object.
 */

SQSStrategy.normalizeOptions = function (options) {
	var opts = options || {};
	opts.maxMessagesPerFetch = opts.maxMessagesPerFetch || 5;
	opts.queryInterval = opts.queryInterval || 1000;
	return opts;
}

/**
 * Starts the timer for queue querying.
 * 	
 * @param  {Function} handleReceiveMessageResult - function responsible to handle message result.
 * @return {void} 
 */

SQSStrategy.prototype._startConsuming = function (handleReceiveMessageResult) {
	if (this._timer !== null) {
		logger.warn('SQSStrategy[startConsuming] - Queue consumer was asked to resume but it is already running');
	}
	logger.info('SQSStrategy[startConsuming] - Resuming queue consuming');

	var self = this;
	this._timer = setInterval(function () {
		self._queryQueue(handleReceiveMessageResult);
	}, this._options.queryInterval);

	self._queryQueue();
}

/**
 * Requeue mesage in the queue (eg. aws-sqs automatically requeue message).
 * 
 * @param  {object}    msg - Message to delete.
 * @return {void}
 */

RabbitStrategy.prototype._nack = function (msg) {}

/**
 * Attempts to delete a message from the queue.
 * 
 * @param  {object}    msg - Message to delete.
 * @param  {Function}  cb - Callback.
 * @return {void}
 */

SQSStrategy.prototype._ack = function(msg, cb) {
	this._service.deleteMessage(this._queueName, msg.ReceiptHandle, function (err) {
		if (err) {
			logger.error('SQSStrategy[ack] - Failed to delete message:', msg, err.stack);	
			cb(err);
			return;
		}
		logger.info('SQSStrategy[ack] - Deleted message', msg);
		cb(err);
	})
}

/**
 * Stops queue consuming.
 * 
 * @param  {void}
 * @return {void}
 */

SQSStrategy.prototype._stop = function() {
	var self = this;

	if (self._timer === null) {
		logger.warn('SQSStrategy[stop] - Queue consmuar was asked to stop but it is not running')
	}
	logger.info('SQSStrategy[stop] - Stopped queue consuming');
	clearInterval(self._timer);
}

/**
 * Query the queue for new messages.
 *
 * @param  {Function}  handleReceiveMessageResult.
 * @return {void}
 */

SQSStrategy.prototype._queryQueue = function (handleReceiveMessageResult) {
	var self = this;

	if (!this._idle) {
		logger.debug("SQSStrategy[queryQueue] - There is still a query in process. Delaying next query.")
		return;
	}

	this.stats.currentCycle++;

	var service = this._service,
		queueName = this._options.queueName,
		self = this;

	logger.info('SQSStrategy[queryQueue] - Querying new messages...');

	this._idle = false;

	service.receiveMessage(queueName, 
			{ MaxNumberOfMessages: self._options.maxMessagesPerFetch },
			function (error, result) {
				self.emit('data', {error : error, messages : result});
			});
}

/**
 * Ignores the messages being processed and reset state to idle.
 * 
 * @return {void}
 */

SQSStrategy.prototype._reset = function () {
	logger.info('SQSStrategy[reset] - Reseting to idle state')
	this._idle = true;
}

/**
 * Convert mesage object to string.
 * 	
 * @param {Object} - msg - Object message.
 * return {String} - msg - String message.
 */

SQSStrategy.prototype._convertMsgToString = function (msg) {
	return ( msg ) ? msg.toString() : '';
}
