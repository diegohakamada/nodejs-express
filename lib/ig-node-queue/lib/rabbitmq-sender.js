var when    = require('when');
var logger 	= require('./log');

/**
 * @class RabbitmqSender
 * 
 * Agent send messages to Rabbotmq.
 * 
 * @param {Opbject}    amqp - a lib of ampq.
 * @param {Opbject}    options - options to send to queue
 *
 * @author Diego Hakamada
 */

function RabbitmqSender (amqp, options) {
	var self      = this;
	self._amqp    = amqp;
	self._options = RabbitmqSender.normalizeOptions(options);
}

RabbitmqSender.normalizeOptions = function (options) {
	var opts = options || {};
	return opts;
}

/**
 *  Send message to exchange/queue exist.
 * 
 * @param  {Object}      msg - message to send
 * @return {callback}    callback
 */

RabbitmqSender.prototype.send = function(msg, callback){
	var self = this;
	
	self._amqp.connect(self._options.amqp.host).then(function (conn) {

		process.once('SIGINT', conn.close.bind(conn));
		
		return when(conn.createChannel().then(function (ch) {
		
			ch.on('error', function(reason) {
				logger.error('RabbitmqSender[send] - ', reason);
				callback(reason);
				return;
			})

			ch.on('close', function() {
				logger.warn('RabbitmqSender[send] - Channel closed');
			})

			return ch.checkExchange(self._options.amqp.mainExchange.name).then(function () {
				return ch.checkQueue(self._options.amqp.mainExchange.queue.name).then(function () {
					ch.publish(self._options.amqp.mainExchange.name, 
							   self._options.amqp.mainExchange.publish.routingKey, 
							   new Buffer(msg), 
							  self._options.amqp.mainExchange.publish.options)
				
					logger.info('RabbitmqSender[send] - Sent message: %s (exchange: %s and routingKey: %s)', msg, self._options.amqp.mainExchange.name, self._options.amqp.mainExchange.routingKey);

					return ch.close();
				})
			})
		
		})).ensure(function () { conn.close(); })

	}).then(function (resolve) {

	if (callback != null)
		callback(null);
	}, 
	function (reject) {
		if (callback != null)
			callback(reject);
	})
}

module.exports = RabbitmqSender;