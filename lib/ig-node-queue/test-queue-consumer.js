var WorkerPool = require('./lib/worker-pool');
var workerPool  = new WorkerPool('test/worker-rabbit', 1, {});
var Sender = require('./lib/rabbitmq-sender');
var sender = new Sender();
var amqp = require('amqplib');
var RabbitmqStrategy = require('./lib/rabbitmq-strategy');
var path = require('path');
var options = require('konfig')({ path: path.resolve('./test/config')}).rabbitmq;
var strategy = new RabbitmqStrategy(amqp, options);

var QueueConsumer = require('./lib/queue-consumer');
var consumer = new QueueConsumer(strategy, workerPool);

consumer.resume();
