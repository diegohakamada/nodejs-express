function RabbitmqSenderMock () {}

RabbitmqSenderMock.prototype.send = function (msg, callback) {
	callback();
}

module.exports = RabbitmqSenderMock;