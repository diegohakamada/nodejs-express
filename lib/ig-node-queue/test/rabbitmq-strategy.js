var AMQPMock			= require('./amqp-mock');
var RabbitmqStrategy 	= require('../lib/rabbitmq-strategy');
var sinon 				= require('sinon');
var assert 				= require('chai').assert;
var Q 					= require('q');
var path 				= require('path');

describe('rabbitmq-strategy module test suite', function () {
	var options	 = require('konfig')({ path: path.resolve('./test/config')}).rabbitmq;

	it('# should send a message to broker', function (done) {

		var connFactory = new AMQPMock();
		var strategy = new RabbitmqStrategy(connFactory, options);

		connFactory.on('createdConn', function (event) {
			connFactory._conn.on('createdChannel', function (event) {
				connFactory._conn._ch.on('createConsume', function (event) {
					connFactory._conn._ch.__send('test message');
				})
			})
		})

		strategy.on('data', function (msg) {
			assert.equal(msg, 'test message', 'receive message');
			done();
		})

		strategy.startConsuming();
	})

	it('# should handle disconnection', function (done) {

		var isReconnecting = false;
		var connFactory = new AMQPMock();
		var strategy = new RabbitmqStrategy(connFactory, options);

		var spyStartConuming = sinon.spy(strategy, 'startConsuming');

		connFactory.on('createdConn', function (event) {

			if(spyStartConuming.calledTwice) {
				connFactory._conn.on('createdChannel', function (event) {
					connFactory._conn._ch.on('createConsume', function (event) {
						connFactory._conn._ch.__send('message after reconnection');
					})
				})
			} else {
				connFactory._conn.on('createdChannel', function (event) {
					connFactory._conn._ch.on('createConsume', function (event) {
						connFactory._conn.__forceDisconnect();
					})
				})
			}
		})

		strategy.on('data', function (msg) {
			assert.equal(msg, 'message after reconnection', 'receive message after reconnection');
			done();
		})

		strategy.startConsuming();
	})
})