var AMQPMock 			= require('./amqp-mock');
var RabbitmqSender 		= require('../lib/rabbitmq-sender');
var sinon 				= require('sinon');
var assert 				= require('chai').assert;
var path                = require('path');

describe('rabbitmq-sender module test suite', function () {

	var message = 'test message';
	var options = require('konfig')({ path: path.resolve('./test/config')}).rabbitmq;

	it('# should send a message to broker', function (done) {

		var connFactory = new AMQPMock();

		var sender = new RabbitmqSender(connFactory, options);

		connFactory.on('createdConn', function (event) {
			connFactory._conn.on('createdChannel', function (event) {
				connFactory._conn._ch.on('createConsume', function (event) {
					connFactory._conn._ch.on('publish', function (event) {
						assert.equal(message, event.message, 'message sent');
					})
				})
			})
		})

		sender.send(message, function (err) {
			assert.isNull(err);
			done();
		})
	})

	it('# should send a message to an exchange that not exists', function (done) {

		var connFactory = new AMQPMock();

		connFactory.on('createdConn', function (event) {
			connFactory._conn.on('createdChannel', function (event) {
				sinon.stub(connFactory._conn._ch, 'checkExchange', function() {
					throw new Error('exchange not exists');
				})
			})
		})
		
		var sender = new RabbitmqSender(connFactory, options);

		sender.send("message", function (err) {
			assert.isNotNull(err, 'exchange not exists');
			done();
		})
	})

	it('# should send a message to an queue that not exists', function (done) {

		var connFactory = new AMQPMock();

		connFactory.on('createdConn', function (event) {
			connFactory._conn.on('createdChannel', function (event) {
				sinon.stub(connFactory._conn._ch, 'checkQueue', function() {
					throw new Error('queue not exists');
				})
			})
		})

		var sender = new RabbitmqSender(connFactory, options);

		sender.send("message", function (err) {
			assert.isNotNull(err, 'exchange not exists');
			done();
		})
	})
})