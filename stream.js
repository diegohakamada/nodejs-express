var Readable = require('stream').Readable;
var util = require('util');
util.inherits(Counter, Readable);

function Counter(opt) {
  Readable.call(this, opt);
  this._max = 15;
  this._index = 1;
}

// Counter.prototype._read = function() {
//   console.log('_read')
//   var i = this._index++;
//   if (i > this._max){
//     console.log('push null')
//     this.resume();
//   }
//   else {
//     var str = '' + i;
//     var buf = new Buffer(str, 'ascii');
//     console.log('push buf', buf.toString()) 
//     this.push(buf);
//   }
// };

Counter.prototype._read = function() {
  console.log('read')
}

// function handleData (msg) {
//   console.log('Received data', msg.toString());
//   if(this._index == 10){
//     console.log('Pausing')
//     this.pause();
//   }
// }


var c = new Counter();
// c.on('data', handleData);

c.on('data', function(chunk) {
  console.log('got %d bytes of data', chunk.length);
});
c.on('end', function() {
  console.log('there will be no more data.');
});

c.push('teste')
c.push('teste123')
c.emit('end')
c.push('teste123')