var cluster = require('cluster');
var wire = require("wire");
var path = require('path');
var logger = require(path.resolve('lib/log.js'));

var context;
var jobs = {};

/**
 * @todo refuse job when busy
 */
wire(require('../../config/context-message-worker'))
	.then(function (ctx) {
		context = ctx;
		// inicializando os jobs
		initJobs();

		logger.info("Worker[messages] Pronto para receber mensagens");

		process.on('message', processNotificaction);

		process.send({ event: 'ready' });

	}, 
	function (e) {
		logger.error('Worker[messages] falhou na inicialização', e.stack);
	})
	.otherwise(function (e) {
		logger.error('Exceção não tratada - matando worker', e.stack || e)
		cluster.worker.kill();
	}
);

function processNotificaction (msg) {
	var msgNotification;

	try {
		msgNotification = JSON.parse(msg.Body);

		logger.info('Worker[messages] Mensagem recebida %s', msgNotification.action);

		var processor = getProcessor(msgNotification.action);

		if (processor) {

			processor._execute(msgNotification, function(err, message) {
				if (err) {

					process.send({ 
							event: 'done', 
							error: err.stack || err, 
							result: { update: message }
						}
					);
					return;
				}

				process.send({ 
					event: 'done', 
					error: null, 
					result: { $return: false }
				});

			});
		} else {
			logger.warn('Worker[messages] Operação não suportada %s', msgNotification.action);
		}
	} catch (e) {
		console.log(e.stack)
		var msgError = 'Worker[messages] Formato de mensagem não reconhecido';
		logger.error(msgError, e.message);
		var err = new Error(msgError + ': ' + e.message);
		process.send({ event: 'done', error: err.stack || err });
		return;
	}

}

function initJobs () {
	jobs.createSubscription = new CreateSubscriptionJob(context);
	jobs.cancelSubscription = new CancelSubscriptionJob(context);
	jobs.overdueSubscription = new OverdueSubscriptionJob(context);
	jobs.changePlan = new ChangePlanJob(context);
}

function getProcessor (action) {
	if (!action) {
		action = 'createSubscription';
	}

	return jobs[action];
}


cluster.worker.on('disconnect', function (msg) {
	logger.info('Worker[messages] Desconectado');
})