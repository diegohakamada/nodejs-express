// var sinon 			= require('sinon');
// var assert 			= require('chai').assert;

// function once(fn) {
//     var returnValue, called = false;
//     return function () {
//         if (!called) {
//             called = true;
//             returnValue = fn.apply(this, arguments);
//         }
//         return returnValue;
//     };
// }

// it('teste spy func', function () {
// 	var spy = sinon.spy(teste);

// 	function teste () {
// 		console.log('teste');
// 	}

// 	spy();

// 	assert(spy.called);
// })

// it('teste spy func start', function () {
// 	var missionImpossible = {
// 	    start: function (agent) {
// 	        agent.apply(this);
// 	    }
// 	};
	 
// 	// By using a sinon.spy(), it allows us to track how the function is used
// 	var ethanHunt = sinon.spy();
// 	missionImpossible.start(ethanHunt);
// 	assert(ethanHunt.called);
// })

// it("should spy on myfunc", function() {
// 	var o = {
// 	    myfunc: function() {
// 	        console.log('hello');
// 	    }
// 	};

//     var mySpy = sinon.spy(o, "myfunc");
//     o.myfunc();

//     sinon.assert.calledOnce(mySpy);
// });

// it("calls the original function", function () {
//     var callback = sinon.spy();
//     var proxy = once(callback);

//     proxy();

//     assert(callback.called);
// });

// it("calls the original function only once", function () {
//     var callback = sinon.spy();
//     var proxy = once(callback);

//     proxy();
//     proxy();

//     assert(callback.calledOnce);
//     // ...or:
//     // assert.equals(callback.callCount, 1);
// });

// it("calls original function with right this and args", function () {
//     var callback = sinon.spy();
//     var proxy = once(callback);
//     var obj = {};

//     proxy.call(obj, 1, 2, 3);

//     assert(callback.calledOn(obj));
//     assert(callback.calledWith(1, 2, 3));
// });









