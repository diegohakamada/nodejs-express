var path              = require('path')
var server 			  = require('./servers/application/application');
var cfg 			  = require('konfig')({ path: path.resolve('config')}).properties;
var cluster 		  = require('cluster');
var async			  = require('async');
const numCPUs 		  = require('os').cpus().length;

// responsável por startar a aplicação

if(cluster.isMaster){
	var cond = 0;

	//fork workers
	async.whilst(
		function () { return cond < numCPUs },
		function (callback){
			cluster.fork();
			++cond;
			callback();
		},
		function (err){
			console.error('Error to initializing the worker', err);
		}
	);

	cluster.on('exit', function(worker, code, signal) {
    	console.log('worker ' + worker.process.pid + ' died');
  	});

  	cluster.on('online', function(worker) {
	  console.log('worker ' + worker.process.pid + ' responded after it was forked');
	});

}else{
	// Workers can share any TCP connection
	server.start(cfg.servers.application.port | 4000 );
}