//A Promise is an object that represents the result of an asynchronous function call
//If a function cannot return a value or throw an exception without blocking, it can return 
//a promise instead. A promise is an object that represents the return value(fulfillment) or the thrown exception(rejected) that the function may eventually provide

var Q = require('q');

function promisedHello() {
	return Q('HELLO')
}

function promisedThrowError () {
	throw 'throw error';
}


/*
 * Q.call - create a promise
 */

// Q.fcall(promisedHello).then(function (value) {
// 	console.log('VALUE:', value)
// })

/*
 * promise with try, catch, finally and fail 
 * if an error in promised will flow all the way to the catch function
 */

// Q.fcall(promisedHello)
// 	.then(promisedThrowError)
// 	.catch(function (error) {
// 	    // Handle any error from all above steps 
// 	    console.log('Error:', error)
// 	})
// 	.done();


// Q.fcall(promisedThrowError)
// 	.then(promisedHello)
// 	.catch(function (error) {
// 	    // Handle any error from all above steps 
// 	    console.log('Error:', error)
// 	})
// 	.done();

//fail  that is like a catch
// var outputPromise = Q().
// 	then(promisedThrowError)
// 	.fail(function (error) {
// 		console.log(error)
// 	}
// );


//fin function that is like a finally clause

// Q()
// .then(promisedThrowError)
// .fin(function () {
// 	console.log('FIN');
// }).then(null, function (rejected) {
// 	console.log('passes to promise:', rejected)
// })

/*
 * then method - fulfillment or rejection handler will always 
 * be called in the next turn of the event loop (i.e. process.nextTick in Node)
 */


/*If the Q() promise gets rejected and you omit the rejection handler, the error will go to outputPromise*/

// var outputPromise = Q()
// .then(function (value) {
// 	throw 'error'
// });

// outputPromise.then(null, function (rejected) {console.log(rejected)})


/*
 * Chaining
 */

 // return promisedHello()
 //        .then(function (value) {
 //        	return value;
 //        })
 //        .then(function (value) {
 //        	console.log('CHAIN:', value)
 //        })


/*
 * Combination
 */

function eventualAdd(a, b) {
    return Q.spread([a, b], function (a, b) {
        return a + b;
    })
}

return Q.all([
    eventualAdd(2, 2),
    eventualAdd(10, 20)
]).then(null, function (value) {
	console.log(value)
});
